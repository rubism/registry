DROP TABLE IF EXISTS products;

CREATE TABLE products
(
    id    INT AUTO_INCREMENT PRIMARY KEY,
    name  VARCHAR(50) NOT NULL,
    price DECIMAL     NOT NULL
);

INSERT INTO products (name, price)
VALUES ('Leche Gloria', 3.40),
       ('Leche Laive', 3.12),
       ('Leche Soy Vida', 2.80);