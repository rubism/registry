package com.productsystem.registry.dto;


import com.productsystem.registry.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductDTO {

    private Product product;
    private ExternalDetails externalDetails;
    private double finalPrice;

}
