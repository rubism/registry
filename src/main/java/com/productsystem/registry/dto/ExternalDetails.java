package com.productsystem.registry.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExternalDetails {

    @JsonProperty("stock")
    private int stock;

    @JsonProperty("discountPercent")
    private double discountPercent;

    @JsonProperty("hasGuarantee")
    private boolean hasGuarantee;

}
