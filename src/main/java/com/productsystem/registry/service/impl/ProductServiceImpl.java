package com.productsystem.registry.service.impl;

import com.productsystem.registry.config.ConfigConstants;
import com.productsystem.registry.dto.ExternalDetails;
import com.productsystem.registry.dto.ProductDTO;
import com.productsystem.registry.entity.Product;
import com.productsystem.registry.exception.ProductRegistryException;
import com.productsystem.registry.mapper.ProductMapper;
import com.productsystem.registry.repository.ProductRepository;
import com.productsystem.registry.service.ProductService;
import com.productsystem.registry.util.Messages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.client.RestTemplate;

@Service
public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;
    private RestTemplate restTemplate;
    private ConfigConstants configConstants;
    private ProductMapper productMapper;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository, RestTemplate restTemplate,
                              ConfigConstants configConstants, ProductMapper productMapper) {
        this.productRepository = productRepository;
        this.restTemplate = restTemplate;
        this.configConstants = configConstants;
        this.productMapper = productMapper;
    }

    @Override
    public Product createProduct(ProductDTO productDTO) {
        Product product = new Product();
        product.setName(productDTO.getProduct().getName());
        product.setPrice(productDTO.getProduct().getPrice());
        productRepository.save(product);
        return product;
    }

    @Override
    @Cacheable("products")
    public ProductDTO getProductById(int id) {
        ProductDTO product = new ProductDTO();
        try {
            Product storedProduct = productRepository.findById(id)
                    .orElseThrow(() -> new ProductRegistryException(Messages.PRODUCT_NOT_FOUND));
            String externalURL = configConstants.getExternalDataUrl();
            ResponseEntity<ExternalDetails> response
                    = restTemplate.getForEntity(externalURL + "/" + id, ExternalDetails.class);
            return productMapper.convertToUserLocationDTO(storedProduct, response.getBody());
        } catch (ProductRegistryException e) {
            e.getCause();
        }
        return product;
    }

    @Override
    public void updateProduct(int id, ProductDTO productDTO) {
        try {
            Product product = productRepository.findById(id)
                    .orElseThrow(() -> new ProductRegistryException(Messages.PRODUCT_NOT_FOUND));
            product.setName(productDTO.getProduct().getName());
            product.setPrice(productDTO.getProduct().getPrice());
            productRepository.save(product);
        } catch (ProductRegistryException e) {
            e.getCause();
        }
    }

}
