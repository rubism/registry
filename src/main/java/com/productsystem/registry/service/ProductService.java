package com.productsystem.registry.service;

import com.productsystem.registry.dto.ProductDTO;
import com.productsystem.registry.entity.Product;
import com.productsystem.registry.exception.ProductRegistryException;

import java.util.List;

public interface ProductService {

    Product createProduct(ProductDTO productDTO) throws ProductRegistryException;

    ProductDTO getProductById(int id) throws ProductRegistryException;

    void updateProduct(int id, ProductDTO productDTO) throws ProductRegistryException;

}
