package com.productsystem.registry.controller;

import com.productsystem.registry.dto.ProductDTO;
import com.productsystem.registry.entity.Product;
import com.productsystem.registry.exception.ProductRegistryException;
import com.productsystem.registry.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = {"/api/products"}, produces = APPLICATION_JSON_VALUE)
public class ProductController {

    @Autowired
    private ProductService productService;

    private static final Logger log = LoggerFactory.getLogger(ProductController.class);

    @PostMapping

    public ResponseEntity<Object> createProduct(@RequestBody ProductDTO productDTO) {
        try {
            log.info("Create new product called: " + productDTO.getProduct().getName());
            Product response = productService.createProduct(productDTO);
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        } catch (ProductRegistryException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable("id") int id) {
        try {
            log.info("Searching by ID  : " + id);
            ProductDTO response = productService.getProductById(id);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (ProductRegistryException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Object> updateProduct(@PathVariable("id") int id,
                                                @RequestBody ProductDTO product) {
        try {
            log.info("Update product with ID: " + id);
            productService.updateProduct(id, product);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (ProductRegistryException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
