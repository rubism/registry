package com.productsystem.registry.mapper;

import com.productsystem.registry.dto.ExternalDetails;
import com.productsystem.registry.dto.ProductDTO;
import com.productsystem.registry.entity.Product;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper {

    public ProductDTO convertToUserLocationDTO(Product product, ExternalDetails externalInfo) {
        return ProductDTO.builder()
                .product(Product.builder()
                        .id(product.getId())
                        .name(product.getName())
                        .price(product.getPrice())
                        .build())
                .externalDetails(ExternalDetails.builder()
                        .discountPercent(externalInfo.getDiscountPercent())
                        .hasGuarantee(externalInfo.isHasGuarantee())
                        .stock(externalInfo.getStock())
                        .build())
                .finalPrice(product.getPrice()*(100-externalInfo.getDiscountPercent())/100)
                .build();
    }

}
