package com.productsystem.registry.exception;

public class ProductRegistryException extends Exception {

    public ProductRegistryException(String message) {
        super(message);
    }

}
