package com.productsystem.registry.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySources({
        @PropertySource("classpath:application-dev.properties"),
        @PropertySource("classpath:application-prod.properties"),
        @PropertySource("classpath:application-test.properties")
})
public class ConfigConstants implements EnvironmentAware {

    @Autowired
    private Environment environment;

    public static final String EXTERNAL_DATA_URL = "products.external-data.url";

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    public String getExternalDataUrl() {
        return environment.getProperty(EXTERNAL_DATA_URL);
    }

}
