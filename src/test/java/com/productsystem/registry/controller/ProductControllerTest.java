package com.productsystem.registry.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.productsystem.registry.dto.ProductDTO;
import com.productsystem.registry.entity.Product;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@ActiveProfiles("test")
@AutoConfigureMockMvc
@SpringBootTest
public class ProductControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    void createProductAPI() throws Exception {
        ProductDTO productDTO = ProductDTO.builder()
                .product(Product.builder()
                        .id(5)
                        .name("Sopa Instantanea Ajinomen")
                        .price(3.20).build())
                .build();

        mvc.perform(
                        MockMvcRequestBuilders.post("/api/products")
                                .content(asJsonString(productDTO))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("name").exists());
    }

    @Test
    void updateProductAPI() throws Exception {
        ProductDTO productDTO = ProductDTO.builder()
                .product(Product.builder()
                        .id(5)
                        .name("Sopa Instantanea Ajinomen")
                        .price(3.20).build())
                .build();

        mvc.perform(MockMvcRequestBuilders
                        .put("/api/products/{id}", 5)
                        .content(asJsonString(productDTO))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
