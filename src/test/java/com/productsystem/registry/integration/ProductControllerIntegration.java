package com.productsystem.registry.integration;

import com.productsystem.registry.RegistryApplication;
import com.productsystem.registry.entity.Product;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = RegistryApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ProductControllerIntegration {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    @Sql(scripts = {"classpath:data.sql"})
    void findProductById() {
        Product product = new Product(1, "Atun Florida", 4.40);
        assertEquals(
                this.restTemplate
                        .getForObject("http://localhost:" + port + "/api/products/1", Product.class), product);
    }

}
