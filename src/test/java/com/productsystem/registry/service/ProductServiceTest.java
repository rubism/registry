package com.productsystem.registry.service;

import com.productsystem.registry.config.ConfigConstants;
import com.productsystem.registry.dto.ProductDTO;
import com.productsystem.registry.entity.Product;
import com.productsystem.registry.exception.ProductRegistryException;
import com.productsystem.registry.mapper.ProductMapper;
import com.productsystem.registry.repository.ProductRepository;
import com.productsystem.registry.service.impl.ProductServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
@ActiveProfiles("test")
@SpringBootTest
class ProductServiceTest {

    @Mock
    private ProductRepository productRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ConfigConstants configConstants;

    @Autowired
    private ProductMapper productMapper;

    ProductService productService;

    @BeforeEach
    void initUseCase() {
        productService = new ProductServiceImpl(productRepository, restTemplate, configConstants, productMapper);
    }

    @Test
    void saveProduct_success() {
        Product product = new Product();
        product.setId(5);
        product.setName("Mermelada A1");
        product.setPrice(5.2);

        when(productRepository.save(any(Product.class))).thenReturn(product);

        Product savedProduct = productRepository.save(product);
        assertNotNull(savedProduct.getName());
    }

    @Test
    void findProductById_success() throws ProductRegistryException {
        Product product = new Product();
        product.setId(4);
        product.setName("Mermelada A1");
        product.setPrice(5.2);

        when(productRepository.findById(4)).thenReturn(Optional.of(product));

        ProductDTO fetchedProducts = productService.getProductById(4);
        assertNotNull(fetchedProducts);
    }

}
