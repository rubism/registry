package com.productsystem.registry.repository;

import com.productsystem.registry.entity.Product;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
@ExtendWith(SpringExtension.class)
class ProductRepositoryTest {

    @Autowired
    private ProductRepository productRepository;

    @BeforeEach
    void initUseCase() {
        List<Product> products = Arrays.asList(
                new Product(10, "Yogurt Gloria", 7.50),
                new Product(11, "Yogurt Laive", 7.20)
        );
        productRepository.saveAll(products);
    }

    @AfterEach
    public void destroyAll() {
        productRepository.deleteAll();
    }

    @Test
    void saveProduct() {
        Product product = new Product(5, "Yogurt Yole", 12.3);
        Product productSaved = productRepository.save(product);

        assertNotNull(productSaved);
    }

    @Test
    void findProductById() {
        Product product = new Product(5, "Yogurt Yole", 12.3);
        productRepository.save(product);
        Optional<Product> productSaved = productRepository.findById(5);

        assertEquals(productSaved.get().getPrice(), 12.3);
        assertEquals(productSaved.get().getName(), "Yogurt Yole");
    }

}
